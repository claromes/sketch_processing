# Python Mode for Processing sketches

## Gallery

- points.py
<img src="./gifs/points.gif">

- square.py
<img src="./gifs/square_2.gif">

- first.py
<img src="./gifs/first.gif">

- sketch_210922a.py
<img src="./gifs/sketch_210922a.gif">

- sketch_210926c.py
<img src="./gifs/sketch_210926c.gif">

- sketch_210919c.py
<img src="./gifs/sketch_210919c.gif">

- sketch_210919b.py
<img src="./gifs/sketch_210919b.gif">
